import os
import gym
import gym_ants
import numpy as np
import datetime
import time
import random
import argparse

# Profiling
import cProfile, pstats, io

from icecream import ic
from tempfile import TemporaryFile
from dateutil.tz import gettz
import torch as th

EXPOSED_LIMBS = {
        "ant": ["torso", "front_left_leg", "front_right_leg", "back_leg", "right_back_leg"],
}

def clip(val, mins, maxs):
    return th.max(th.min(val, maxs), mins)

def profile(fn):
    def inner(*args, **kwargs):
        pr = cProfile.Profile()
        pr.enable()
        res = fn(*args, **kwargs)
        pr.disable()
        s = io.StringIO()
        ps = pstats.Stats(pr, stream=s).sort_stats('cumulative')
        ps.print_stats()
        print(s.getvalue())
        return res
    return inner

GOAL_BOUNDS_DIR = "./goal_bounds"
if not os.path.exists(GOAL_BOUNDS_DIR):
    os.makedirs(GOAL_BOUNDS_DIR)

def get_goal_bounds(env_name):
    path = os.path.join(GOAL_BOUNDS_DIR, env_name+".npy")
    try:
        return th.from_numpy(np.load(path))
        # with open(path, "wb") as f:
            # return np.load(f)
    except:
        bounds = calc_goal_bounds(env_name)
        np.save(path, bounds)
        return th.from_numpy(bounds)

def calc_goal_bounds(env_name, iterations = 100):
    print("Calculating {} state change bounds. Running the env {} times".format(env_name, iterations))
    env = gym.make(env_name)
    obs = env.reset()
    upper_bounds = obs - obs
    lower_bounds = obs - obs
    last_obs = obs
    for _ in range(iterations):
        obs = env.reset()
        done = False
        t = 0
        while not done:
            action = env.action_space.sample()
            obs, _, done, _ = env.step(action)
            if t % 10 == 0:
                diff = obs - last_obs
                u_mask = np.where(diff > upper_bounds)
                upper_bounds[u_mask] = diff[u_mask]
                l_mask = np.where(diff < lower_bounds)
                lower_bounds[l_mask] = diff[l_mask]
                last_obs = obs
            t += 1
    return np.vstack((upper_bounds, lower_bounds))

STAT_DIR = "./state_stats"
if not os.path.exists(STAT_DIR):
    os.makedirs(STAT_DIR)

def get_stats(env_name):
    path = os.path.join(STAT_DIR, env_name+".npz")
    try:
        out = np.load(path)
        return th.from_numpy(out["mean"]), th.from_numpy(out["std"])
        # with open(path, "wb") as f:
            # return np.load(f)
    except:
        mean, std = calc_stats(env_name)
        zeros = np.where(np.isclose(std, 0))
        std[zeros] = 1
        np.savez(path, mean=mean, std=std)
        return th.from_numpy(mean), th.from_numpy(std)

def calc_stats(env_name, iterations = 100):
    print("Calculating {} state bounds. Running the env {} times".format(env_name, iterations))
    env = gym.make(env_name)
    obs = env.reset()
    obs_arr = []
    for _ in range(iterations):
        obs = env.reset()
        done = False
        while not done:
            action = env.action_space.sample()
            obs, _, done, _ = env.step(action)
            obs_arr.append(obs)
    return np.mean(obs_arr, axis=0), np.std(obs_arr, axis=0)

def parse_args():
    parser = argparse.ArgumentParser()
    # For logs
    parser.add_argument("--run", default=None)
    parser.add_argument("--env_name", default="HalfCheetah-v2")
    parser.add_argument("--seed", default=-1, type=int)
    # How many time steps purely random policy is run for
    parser.add_argument("--start_timesteps", default=1e4, type=int)
    # How often (time steps) we evaluate
    parser.add_argument("--eval_freq", default=5e3, type=float)
    # Max time steps to run environment for
    parser.add_argument("--max_timesteps", default=1e7, type=float)
    # Whether or not models are saved
    parser.add_argument("--save_models", action="store_true")
    # Std of Gaussian exploration noise for the high level policy. Scaled down because we scaled down the goal
    parser.add_argument("--hl_expl_noise", default=1, type=float)
    # Std of Gaussian exploration noise for the low level policy
    parser.add_argument("--ll_expl_noise", default=1, type=float)
    # Batch size for both actor and critic
    parser.add_argument("--batch_size", default=128, type=int)
    # Low level policy discount factor
    parser.add_argument("--ll_discount", default=0.99, type=float)
    # High level policy discount factor
    parser.add_argument("--hl_discount", default=0.99, type=float)
    # Target network update rate
    parser.add_argument("--tau", default=0.005, type=float)
    # Noise added to target policy during critic update
    parser.add_argument("--policy_noise", default=0.2, type=float)
    # Range to clip target policy noise
    parser.add_argument("--noise_clip", default=0.5, type=float)
    # Frequency of delayed policy updates
    parser.add_argument("--policy_freq", default=10, type=int)
    parser.add_argument("--logging_period", default=10, type=int)
    # Specifies the prefix of the model to load
    parser.add_argument("--load", default=None)
    # Render training
    parser.add_argument("--render_all", dest='render_all', action='store_true')
    # Don't render eval
    parser.add_argument("--norender", dest='render', action='store_false')
    parser.set_defaults(render=True)
    parser.set_defaults(render_all=False)
    parser.set_defaults(load=False)
    args = parser.parse_args()

    # Set defaults
    if args.seed == -1:
        args.seed = random.randint(0, 1000000)

    unix_time = time.time()
    dt = datetime.datetime.fromtimestamp(unix_time, gettz("America/Los_Angeles"))
    if not args.run:
        if args.load:
            args.run =  args.load
        else:
            args.run = dt.isoformat()
    else:
        args.run = args.run

    args.logdir = os.path.join("./logs", args.run)
    if not os.path.exists(args.logdir):
        os.makedirs(args.logdir)


    args.filename = "%s_%s" % (args.run, str(args.seed))
    return args
