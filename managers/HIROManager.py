import numpy as np
import torch as th
import random
import math
import utils
from icecream import ic
import pickle

from replaybuffer import ReplayBuffer
from models import HIRO

device = th.device("cuda" if th.cuda.is_available() else "cpu")

# Idea: Prevent the network from exploring less when network outputs max
def give_room(output, room, max_out, min_out):
    h_clipped = th.where(output + room > max_out)
    l_clipped = th.where(output - room < min_out)
    output[h_clipped] -= room
    output[l_clipped] += room
    return output


# Policy agnostic HRL replay and training manager
class HIROManager:

    MAX_GOAL = 1

    # the only two stateful components of this manager is the network
    def __init__(self, policy, logger, state_dim, action_dim, goal_bounds, max_action,
                 state_stats, params):
        self.logger = logger
        self.goal_bounds = goal_bounds
        self.params = params
        self.action_dim = action_dim
        self.goal_dim = goal_bounds.size()[1]
        self.max_action = max_action

        self.ll_replay_buffer = ReplayBuffer()
        self.hl_replay_buffer = ReplayBuffer()
        self.reset_replay()

        self.policy = policy #HIRO.HIRO(state_dim, action_dim, goal_bounds, state_stats, max_action-10, HIROManager.MAX_GOAL)

        self.action_noise = th.normal(0, th.ones(self.action_dim)*self.params.hl_expl_noise).to(device)
        self.action_noise_n = 1
        self.goal_noise = th.normal(0, th.ones(self.goal_dim)*self.params.hl_expl_noise).to(device)
        self.goal_noise_n = 1

    # TODO move to HIRO
    def random_goal(self):
        rand = HIROManager.MAX_GOAL*th.normal(0, th.ones((1, self.goal_dim))).to(device)
        return self.policy.hl_policy.scale(rand)

    def get_ll_expl_noise(self, total_timesteps, reset):
        std = max((0.8**(total_timesteps/1e6))*self.params.ll_expl_noise, 0.5)
        if reset:
            self.action_noise_n = 0
            self.action_noise = th.zeros_like(self.action_noise)
        self.action_noise_n += 1
        self.action_noise += th.normal(0, std*th.ones((self.action_dim))).to(device)
        return th.normal(0, std*th.ones((self.action_dim))).to(device)
        # return self.action_noise / math.sqrt(self.action_noise_n)

    def get_hl_expl_noise(self, total_timesteps, reset):
        std = max((0.8**(total_timesteps/1e6))*self.params.hl_expl_noise, 0.5)
        if reset:
            self.goal_noise_n = 0
            self.goal_noise = th.zeros_like(self.goal_noise).to(device)
        # Add exploration noise
        self.goal_noise_n += 1
        self.goal_noise += th.normal(0, std*th.ones((self.goal_dim))).to(device)
        return th.normal(0, std*th.ones((self.goal_dim))).to(device)
        # return self.goal_noise / math.sqrt(self.action_noise_n)

    def step(self, state, goal, env, episode_timesteps, total_timesteps):

        # Goal is updated before the action
        action = self.policy.select_action(state, goal)
        o_action = action.clone()
        noise = self.get_ll_expl_noise(total_timesteps, episode_timesteps % self.params.policy_freq == 0)
        action += noise
        # Torch has no equivalent
        action = th.from_numpy(action.cpu().data.numpy().clip(env.action_space.low, env.action_space.high))
        # print(action, noise, o_action)

        # RUN ENV
        new_state, reward, done, info = env.step(action)
        new_state = th.FloatTensor(new_state).to(device)
        reward = reward
        done = done

        # Compute goal for next state
        # Update goal using policy every policy_freq steps otherwise use transition
        if episode_timesteps % self.params.policy_freq == 0:
            new_goal = self.policy.select_goal(new_state)
            # new_goal += self.get_hl_expl_noise(total_timesteps, episode_timesteps % env._max_episode_steps == 0)
            # print(new_goal)
            # Clip goal
        else:
            # Add noise every time, or just when selecting?
            # new_goal = (goal + th.normal(0, self.params.expl_noise,
                    # size=self.goal_dim))
            # # clamp goal
            # for i, val in enumerate(new_goal):
                # new_goal[i] = th.clamp(val, -self.goal_bounds[i], self.goal_bounds[i])
            new_goal = self.policy.interp_goal(state, new_state, goal)

        for i, val in enumerate(new_goal):
            new_goal[i] = th.clamp(val, self.goal_bounds[1,i], self.goal_bounds[0,i])

        return new_state, reward, done, info, action, new_goal


    def train_hl(self, update_target):
        if len(self.hl_replay_buffer.storage) < self.params.batch_size:
            return
        # Train high level self.policy
        sample_batch = self.hl_replay_buffer.sample(self.params.batch_size)
        critic_loss, actor_loss = self.policy.train_hl(
            sample_batch, update_target, self.params.policy_freq, self.params.hl_discount,
            self.params.tau, self.params.policy_noise, self.params.noise_clip)
        self.logger.log_avg_scalar("HL/Critic Loss", critic_loss, window=self.params.logging_period)
        if actor_loss:
            self.logger.log_avg_scalar("HL/Actor Loss", actor_loss, window=self.params.logging_period)
        return critic_loss, actor_loss


    def train_ll(self, update_target):
        if len(self.ll_replay_buffer.storage) < self.params.batch_size:
            return
        # Train low level self.policy
        sample_batch = self.ll_replay_buffer.sample(self.params.batch_size)

        ll_reward, critic_loss, actor_loss = self.policy.train_ll(
            sample_batch, update_target, self.params.ll_discount,
            self.params.tau, self.params.policy_noise, self.params.noise_clip)
        self.logger.log_avg_scalar("LL/Critic Loss", critic_loss, window=self.params.logging_period*self.params.policy_freq)
        if actor_loss:
            self.logger.log_avg_scalar("LL/Actor Loss", actor_loss, window=self.params.logging_period*self.params.policy_freq)
        self.logger.log_avg_scalar("LL/Reward", ll_reward, window=self.params.logging_period*self.params.policy_freq)
        return ll_reward, critic_loss, actor_loss


    def add_replay(self, episode_timesteps, state, new_state, goal, new_goal, reward, done_bool, action):
        # state -> {select_goal} -> goal
        # state, goal -> {select_action} -> action
        # action -> {env} -> reward
        # state = s_t
        # goal = g_t
        # new_goal = g_t+1
        # reward = r_t
        # action = u_t
        # new_state = s_t+1
        action = th.squeeze(action)
        self.ll_replay_buffer.add((state, new_state, goal.squeeze(),
                                   new_goal.squeeze(), done_bool, action, reward))
        if episode_timesteps % self.params.policy_freq == 0 and self.state_seq:
            self.hl_replay_buffer.add((self.state_seq, new_state, self.goal_seq,
                                       self.reward_seq, done_bool, self.action_seq))
            self.state_seq = []
            self.goal_seq = []
            self.reward_seq = []
            self.action_seq = []
        else:
            # For the first iteration, make sure there are not too many elements
            if len(self.state_seq) >= self.params.policy_freq-1:
                self.state_seq.pop()
                self.goal_seq.pop()
                self.reward_seq.pop()
                self.action_seq.pop()
            # Record sequences for hl self.policy
            self.state_seq.append(state)
            self.goal_seq.append(goal.squeeze())
            self.reward_seq.append(reward)
            self.action_seq.append(action)

    def reset_replay(self):
        self.state_seq = []
        self.goal_seq = []
        self.reward_seq = []
        self.action_seq = []

    def save(self, filepath):
        with open(filepath, "wb+") as f:
            pickle.dump(self, f)

    def load(self, filepath):
        self = pickle.load(filepath)
