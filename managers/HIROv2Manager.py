import numpy as np
import torch as th
import random
import math
import utils
from icecream import ic
import pickle

from replaybuffer import ReplayBuffer
from models import HIRO
from .HIROManager import HIROManager

device = th.device("cuda" if th.cuda.is_available() else "cpu")

# Policy agnostic HRL replay and training manager
class HIROv2Manager(HIROManager):

    def train_ll(self, update_target):
        if len(self.ll_replay_buffer.storage) < self.params.batch_size:
            return
        # Train low level self.policy
        sample_batch = self.ll_replay_buffer.sample(self.params.batch_size)

        ll_reward, repr_loss, critic_loss, actor_loss = self.policy.train_ll(
            sample_batch, update_target, self.params.ll_discount,
            self.params.tau, self.params.policy_noise, self.params.noise_clip)
        self.logger.log_avg_scalar("LL/Critic Loss", critic_loss, window=self.params.logging_period*self.params.policy_freq)
        if actor_loss:
            self.logger.log_avg_scalar("LL/Actor Loss", actor_loss, window=self.params.logging_period*self.params.policy_freq)
        self.logger.log_avg_scalar("LL/Reward", ll_reward, window=self.params.logging_period*self.params.policy_freq)
        self.logger.log_avg_scalar("Repr Loss", repr_loss, window=self.params.logging_period*self.params.policy_freq)
        return ll_reward, critic_loss, actor_loss

    def add_replay(self, episode_timesteps, state, new_state, goal, new_goal, reward, done_bool, action):
        # state -> {select_goal} -> goal
        # state, goal -> {select_action} -> action
        # action -> {env} -> reward
        # state = s_t
        # goal = g_t
        # new_goal = g_t+1
        # reward = r_t
        # action = u_t
        # new_state = s_t+1
        action = th.squeeze(action)
        if episode_timesteps % self.params.policy_freq == 0 and self.state_seq:
            self.hl_replay_buffer.add((self.state_seq, new_state, self.goal_seq, new_goal,
                                       self.reward_seq, done_bool, self.action_seq))
            self.state_seq = []
            self.goal_seq = []
            self.reward_seq = []
            self.action_seq = []
        else:
            # For the first iteration, make sure there are not too many elements
            if len(self.state_seq) >= self.params.policy_freq-1:
                self.state_seq.pop()
                self.goal_seq.pop()
                self.reward_seq.pop()
                self.action_seq.pop()
            # Record sequences for hl self.policy
            self.state_seq.append(state)
            self.goal_seq.append(goal.squeeze())
            self.reward_seq.append(reward)
            self.action_seq.append(action)

    def reset_replay(self):
        self.ll_replay_buffer = self.hl_replay_buffer
        self.state_seq = []
        self.goal_seq = []
        self.reward_seq = []
        self.action_seq = []
