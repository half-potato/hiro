# Ideas
The higher level policy is sort of like the cortex, and lower level policy is like the cerebellum
The cerebellum remembers things for the rest of your life. You never forget how to ride a bike
However, the higher level policy is highly subject to change

Fix replay buffer

# Installing MuJoCo
Obtain license and copy to ~/.mujoco/mjkey.txt
Download 1.50 to ~/.mujoco/mjpro150
Install libs
```
sudo add-apt-repository ppa:xorg-edgers/ppa
sudo add-apt-repository ppa:jamesh/snap-support
sudo apt install patchelf libosmesa6-dev
```

Requirements:
* MuJoCo
* Garage https://github.com/rlworkgroup/garage (clone and install, it's not on any pm)

Code for performing Hierarchical RL based on
"Data-Efficient Hierarchical Reinforcement Learning" by
Ofir Nachum, Shixiang (Shane) Gu, Honglak Lee, and Sergey Levine
(https://arxiv.org/abs/1805.08296).

The reason the goal goes the max is because the higher the goal is, the further the ant moves. 
