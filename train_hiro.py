import numpy as np
import torch as th
import gym
import os
import gym_ants
import random
import utils

# Profiling
import cProfile, pstats, io

from dateutil.tz import gettz
from icecream import ic

# from utils import LLReplayBuffer, HLReplayBuffer, ReplayBuffer
from models import HIRO, HIROv2
from managers.HIROManager import HIROManager
from managers.HIROv2Manager import HIROv2Manager
from tensorboard_api import Tensorboard

device = th.device("cuda" if th.cuda.is_available() else "cpu")

def get_disp_pos(goal, obs):
    goal = goal.reshape(1, -1)
    goal_w = goal.shape[1]
    return np.hstack(((goal+obs[:goal_w]).cpu().data.numpy(), np.zeros((1, max(0, 3 - goal_w)))))[:,:3]

if __name__ == "__main__":

    args = utils.parse_args()

    if not os.path.exists("./results"):
        os.makedirs("./results")
    if args.save_models and not os.path.exists("./pytorch_models"):
        os.makedirs("./pytorch_models")

    logger = Tensorboard(args.logdir)

    th.manual_seed(args.seed)
    np.random.seed(args.seed)
    env = gym.make(args.env_name)
    # def new_get_obs(self):
        # return np.concatenate([
            # self.sim.data.qpos.flat,
            # self.sim.data.qvel.flat,
        # ])
    # env.unwrapped._get_obs = new_get_obs
    env.unwrapped.expose_pos = True

    # ===========================================================================

    # Runs policy for X episodes and returns average reward
    def evaluate_policy(manager, eval_episodes=10):
        avg_reward = 0.
        ll_reward = 0.
        for _ in range(eval_episodes):
            obs = th.FloatTensor(env.reset()).to(device)
            done = False
            t = 0
            goal = manager.policy.select_goal(obs)
            while not done:
                action = manager.policy.select_np_action(obs, goal)
                new_state, reward, done, _ = env.step(action)
                new_state = th.FloatTensor(new_state).to(device)
                # Update goal based on hl self.policy
                if t % args.policy_freq == 0:
                    goal = manager.policy.select_goal(new_state)
                else:
                    goal = manager.policy.interp_goal(obs, new_state, goal)
                if args.render:
                    env.render()
                    env.unwrapped.viewer.add_marker(pos=get_disp_pos(goal, new_state), label="Goal")
                # ll_reward += manager.policy.ll_reward(obs, new_state, goal)
                obs = new_state
                avg_reward += reward
                t += 1

        avg_reward /= eval_episodes
        ll_reward /= eval_episodes
        logger.log_scalar("Eval/HL Reward", avg_reward)
        logger.log_scalar("Eval/LL Reward", ll_reward)

        print("---------------------------------------")
        print("Evaluation over %d episodes: %f, LL: %f" % (eval_episodes, avg_reward, ll_reward))
        print("---------------------------------------")
        return avg_reward

    # ===========================================================================

    # Set seeds
    env.seed(args.seed)
    
    obs = th.FloatTensor(env.reset()).to(device)
    state_dim = obs.shape[0]
    action_dim = env.action_space.shape[0]
    max_action = float(env.action_space.high[0])

    if "Ant" in args.env_name:
        goal_bounds = th.FloatTensor([[10, -10], [10, -10], [0.5, -0.5], # x y z pos lims
                                      [1, -1], [1, -1], [1, -1], [1, -1],
                                      [0.5, -0.5], [0.3, -0.3],
                                      [0.5, -0.5], [0.3, -0.3],
                                      [0.5, -0.5], [0.3, -0.3],
                                      [0.5, -0.5], [0.3, -0.3]]).t().to(device) # orientation lims
        # goal_bounds = th.tensor([[10, -10], [10, -10], [0.5, -0.5]]).t
        # goal_bounds = th.FloatTensor([[10, -10], [10, -10]]).t().to(device)
        goal_bounds = th.FloatTensor([[20, -4], [20, -4]]).t().to(device) # for maze
    else:
        # goal_bounds = utils.get_goal_bounds(args.env_name)
        goal_bounds = th.FloatTensor([[10, -10]]).t().to(device)

    # policy = HIRO.HIRO(state_dim, action_dim, goal_bounds, state_stats, max_action-10, HIROManager.MAX_GOAL)
    policy = HIROv2.HIROv2(state_dim, action_dim, max_action, HIROManager.MAX_GOAL)
    # hiro_manager = HIROManager(policy, logger, state_dim, action_dim,
            # goal_bounds, max_action, utils.get_stats(args.env_name), args)
    hiro_manager = HIROv2Manager(policy, logger, state_dim, action_dim,
            goal_bounds, max_action, utils.get_stats(args.env_name), args)

    if args.load:
        hiro_manager.policy.load(args.load)

    # Evaluate untrained policy
    evaluations = []

    total_timesteps = 0
    timesteps_since_eval = 0
    episode_num = 0
    episode_timesteps = 0
    episode_reward = 0
    episode_ll_reward = 0
    episode_ll_loss = 0
    done = True

    state = th.FloatTensor(env.reset()).to(device)
    goal = th.zeros_like(hiro_manager.random_goal()).to(device)
    action = hiro_manager.policy.select_action(state, goal)
    # Prev states for use in initialization

    while total_timesteps < args.max_timesteps:
        if args.render_all:
            env.render()

        if done:
            if total_timesteps != 0:
                print("Total T: %d Episode Num: %d Episode T: %d Reward: %f LLReward: %f, LLLoss: %f" % \
                      (total_timesteps, episode_num, episode_timesteps, episode_reward, episode_ll_reward, episode_ll_loss))

                # logger.log_scalar("Timesteps", episode_timesteps)
                logger.log_scalar("HL/Reward", episode_reward)
            # if total_timesteps > args.batch_size:
                # for it in range(args.batch_size):
                    # hiro_manager.train_ll(it % 2 == 0)
                # for it in range(args.batch_size//10):
                    # hiro_manager.train_hl(it % 2 == 0)

            # Evaluate episode
            if timesteps_since_eval >= args.eval_freq:
                timesteps_since_eval %= args.eval_freq
                evaluations.append(evaluate_policy(hiro_manager))

                if args.save_models:
                    hiro_manager.policy.save(os.path.join("./pytorch_models", args.filename))
                # th.save("./results/%s" % (args.filename), evaluations)

            # Reset environment
            state = th.FloatTensor(env.reset()).to(device)
            goal = th.zeros_like(goal).to(device)
            done = False
            hiro_manager.reset_replay()
            episode_reward = 0
            episode_timesteps = 0
            episode_num += 1
            episode_ll_reward = 0
            episode_ll_loss = 0

            logger.log_scalar("Misc/HL Buffer Len", len(hiro_manager.hl_replay_buffer))
            logger.log_scalar("Misc/LL Buffer Len", len(hiro_manager.ll_replay_buffer))

        done_bool = float(0) if episode_timesteps + 1 == env._max_episode_steps else float(done)
        """
        # Random actions are preformed to initialize the policy
        if total_timesteps < args.start_timesteps/2:
            # Init ll
            new_state, reward, done, info, action, new_goal = hiro_manager.ll_init_step(
                    state, goal, env, episode_timesteps)
            hiro_manager.add_ll_replay(episode_timesteps, state, new_state, goal,
                                       new_goal, reward, done_bool, action)
            hiro_manager.train_ll(total_timesteps % 2 == 0)
        elif total_timesteps < args.start_timesteps:
            # Init hl
            new_state, reward, done, info, action, new_goal = hiro_manager.hl_init_step(
                    state, goal, env)
            if episode_timesteps > 0:
                # Add previous stuff to replay now that we have the next goal
                hiro_manager.add_replay(episode_timesteps, prev_state, state, goal,
                                        new_goal, reward, done_bool, prev_action)
            hiro_manager.train_ll(total_timesteps % 2 == 0)
            if episode_timesteps % 10 == 0:
                hiro_manager.train_hl(total_timesteps % 20 == 0)
        else:
            # Select action and apply it to env
            new_state, reward, done, info, action, new_goal = hiro_manager.step(
                    prev_state, state, goal, env, episode_timesteps, total_timesteps)
            # Add to replay
            hiro_manager.add_replay(episode_timesteps, state, new_state, goal,
                                    new_goal, reward, done_bool, action)
            hiro_manager.train_ll(total_timesteps % 2 == 0)
            if episode_timesteps % 10 == 0:
                hiro_manager.train_hl(total_timesteps % 20 == 0)
        """
        # Select action and apply it to env
        new_state, reward, done, info, action, new_goal = hiro_manager.step(
            state, goal, env, episode_timesteps, total_timesteps)
        # state -> new_goal
        # new_goal + state -> action -> new_state
        # if the done_bool is set, the network ignores next_state
        # hiro_manager.add_replay(episode_timesteps, state, new_state, goal,
                                # new_goal, reward, done_bool, action)
        hiro_manager.add_replay(episode_timesteps, state, new_state, goal,
                                new_goal, reward, done_bool, action)
        logger.log_avg_scalar("Misc/Avg Action Val", th.mean(action), window=args.logging_period)
        logger.log_avg_scalar("Misc/Avg Goal Val", th.mean(new_goal), window=args.logging_period)
        if episode_num > 5:
            ll_reward, critic_loss, actor_loss = hiro_manager.train_ll(total_timesteps % 2 == 0)
            episode_ll_reward = episode_ll_reward*args.ll_discount + ll_reward
            if actor_loss is not None:
                episode_ll_loss += actor_loss
            if episode_timesteps % args.policy_freq == 0:
                hiro_manager.train_hl(episode_timesteps % 20 == 0)
        # ic(state[:1], new_state[:1], goal)
        # ic("reward", hiro_manager.policy.ll_reward(state, new_state, goal))

        if env.unwrapped.viewer and args.render_all:
            dp = get_disp_pos(hiro_manager.policy.select_goal(new_state), new_state)
            env.unwrapped.viewer.add_marker(pos=dp, label="Selected goal")
            env.unwrapped.viewer.add_marker(pos=get_disp_pos(new_goal, new_state), label="Goal")
            # goals = hiro_manager.policy.get_sample_goals(state.reshape(1,-1), new_state.reshape(1,-1), hiro_manager.random_goal(), 10)
            # for i, g in enumerate(goals):
                # env.unwrapped.viewer.add_marker(pos=get_disp_pos(g, new_state), label="Samp g:{}".format(i))
                
            # real_goal = (env.unwrapped.goal_sample_fn())
            # real_goal = np.hstack((real_goal, [0]))
            # env.unwrapped.viewer.add_marker(pos=real_goal, label="True goal")


        # Update vars
        state = new_state
        goal = new_goal

        episode_reward = episode_reward*args.hl_discount + reward
        episode_timesteps += 1
        total_timesteps += 1
        timesteps_since_eval += 1
        
    # Final evaluation 
    evaluations.append(evaluate_policy(hiro_manager.policy))
    if args.save_models:
        hiro_manager.policy.save("%s" % (args.filename), directory="./pytorch_models")
    # np.save("./results/%s" % (args.filename), evaluations)  

