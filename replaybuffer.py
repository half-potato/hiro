import numpy as np
import torch as th
from icecream import ic
import pickle

device = th.device("cuda" if th.cuda.is_available() else "cpu")

def shapetype(dat):
    if type(dat) == np.ndarray:
        return dat.shape
    elif type(dat) == th.Tensor:
        return dat.shape
    else:
        return type(dat)

def squeeze(x):
    if type(x) == np.ndarray:
        return th.tensor(np.squeeze(x).reshape(1, -1))
    elif type(x) == th.Tensor:
        return x.squeeze().reshape(1, -1)
    elif type(x) == list and type(x[0]) == th.Tensor:
        return th.stack(x, dim=0).reshape(1, len(x), -1)
    return x

def cat(x):
    if type(x[0]) == th.Tensor:
        return th.cat(x, dim=0).to(device)
    else:
        return th.from_numpy(np.array(x)).to(device).type(th.float)

class ReplayBuffer(object):
    def __init__(self, max_size=200000, initial_shape=None):
        self.storage = []
        self.ind = 0
        self.max_size = max_size
        self.initial_shape = initial_shape

    def validate_shape(self, data):
        if self.initial_shape is None:
            return True
        else:
            return not False in [shapetype(x) == y for x, y in zip(data, self.initial_shape)]

    # Expects tuples of (state, next_state, action, reward, done)
    def add(self, data):
        data = [squeeze(x) for x in data]
        if self.initial_shape is None:
            self.initial_shape = [shapetype(x) for x in data]

        if not self.validate_shape(data):
            print([shapetype(x) for x in data], self.initial_shape)
            assert self.validate_shape(data)
        if self.ind >= len(self.storage):
            self.storage.append(data)
        else:
            self.storage[self.ind] = data
        self.ind = (self.ind+1) % self.max_size

    def sample(self, batch_size=100):
        if batch_size > len(self.storage):
            batch_size = len(self.storage)
        return self.index(np.random.randint(0, len(self.storage), size=batch_size))

    def index(self, indices):
        size = len(self.storage[0])

        outs = [[] for i in range(size)]

        for i in indices:
            elements = self.storage[i]
            for j, v in enumerate(elements):
                outs[j].append(v)

        for i, v in enumerate(outs):
            outs[i] = cat(v).detach()

        return outs

    def __len__(self):
        return len(self.storage)

    def save(self, filepath):
        with open(filepath, "wb+") as f:
            pickle.dump(self.storage, f)

    def load(self, filepath):
        self.storage = pickle.load(filepath)
