import HIRO

# Tests that observations and actions look the same everywhere
# Everywhere:
#  ReplayBuffer
#  select_action
#  select_goal
#  train_ll
#  train_hl

parser = argparse.ArgumentParser()
parser.add_argument("--env_name", default="HalfCheetah-v2")
parser.add_argument("--seed", default=0, type=int)

def collect_train():
    pass
def collect_eval():
    pass

    while total_timesteps < args.max_timesteps:
        if args.render_all:
            env.render()

        if done:
            if total_timesteps != 0:
                print("Total T: %d Episode Num: %d Episode T: %d Reward: %f LLReward: %f" % \
                      (total_timesteps, episode_num, episode_timesteps, episode_reward, episode_ll_reward))

                # logger.log_scalar("Timesteps", episode_timesteps)
                logger.log_scalar("Reward", episode_reward)
            if total_timesteps > args.batch_size:
                for it in range(args.batch_size):
                    hiro_manager.train_ll(it % 2 == 0)
                for it in range(args.batch_size//10):
                    hiro_manager.train_hl(it % 2 == 0)

            # Evaluate episode
            if timesteps_since_eval >= args.eval_freq:
                timesteps_since_eval %= args.eval_freq
                evaluations.append(evaluate_policy(hiro_manager))

                if args.save_models:
                    hiro_manager.policy.save(file_name, directory="./pytorch_models")
                np.save("./results/%s" % (file_name), evaluations)

            # Reset environment
            state = env.reset()
            done = False
            episode_reward = 0
            episode_timesteps = 0
            episode_num += 1
            episode_ll_reward = 0

        # Select action and apply it to env
        new_obs, reward, done, info, action, new_goal = hiro_manager.step(
                state, goal, env, episode_timesteps, total_timesteps)
        # ic(state[:1], new_obs[:1], goal)
        # ic("reward", hiro_manager.policy.ll_reward(state, new_obs, goal))

        if env.unwrapped.viewer and args.render_all:
            env.unwrapped.viewer.add_marker(pos=get_disp_pos(new_goal, new_obs), label="Goal")

        logger.log_avg_scalar("Avg Action Val", np.mean(action), window=1000)
        logger.log_avg_scalar("Avg Goal Val", np.mean(new_goal), window=1000)

        # Add to replay
        done_bool = 0 if episode_timesteps + 1 == env._max_episode_steps else float(done)
        hiro_manager.add_replay(state, new_obs, goal, new_goal, reward, done_bool, action)
