import gym
from gym import error, spaces, utils
from gym.utils import seeding
# from gym_ants.envs.ant_goal import EnvWithGoal
from . import ant_goal
from .ant_goal import EnvWithGoal

class AntMazeEnv(EnvWithGoal):
    ANT_ENV_TYPE = 'AntMaze'

class AntPushEnv(EnvWithGoal):
    ANT_ENV_TYPE = 'AntPush'

class AntFallEnv(EnvWithGoal):
    ANT_ENV_TYPE = 'AntFall'
