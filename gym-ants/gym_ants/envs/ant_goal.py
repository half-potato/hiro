import numpy as np
# import gym_ants.envs.maze_env as MazeEnv
from . import maze_env
from .maze_env import MazeEnv

def get_goal_sample_fn(env_name, random=True):
    if env_name == 'AntMaze':
        # NOTE: When evaluating (i.e. the metrics shown in the paper,
        # we use the commented out goal sampling function.    The uncommented
        # one is only used for training.
        if random:
            return lambda: np.random.uniform((-4, -4), (20, 20))
        else:
            return lambda: np.array([0., 16.])
    elif env_name == 'AntPush':
        return lambda: np.array([0., 19.])
    elif env_name == 'AntFall':
        return lambda: np.array([0., 27., 4.5])
    else:
        assert False, 'Unknown env'


def get_reward_fn(env_name):
    if env_name == 'AntMaze':
        return lambda obs, goal: -np.sum(np.square(obs[:2] - goal)) ** 0.5
    elif env_name == 'AntPush':
        return lambda obs, goal: -np.sum(np.square(obs[:2] - goal)) ** 0.5
    elif env_name == 'AntFall':
        return lambda obs, goal: -np.sum(np.square(obs[:3] - goal)) ** 0.5
    else:
        assert False, 'Unknown env'

class EnvWithGoal(MazeEnv):
    ANT_ENV_TYPE = ""

    def __init__(self, random=False, norm='None', *args, **kwargs):
        ANT_ENV_TYPE = self.__class__.ANT_ENV_TYPE
        maze_id = None
        if ANT_ENV_TYPE.startswith('AntMaze'):
            maze_id = 'Maze'
        elif ANT_ENV_TYPE.startswith('AntPush'):
            maze_id = 'Push'
        elif ANT_ENV_TYPE.startswith('AntFall'):
            maze_id = 'Fall'
        else:
            raise ValueError('Unknown maze environment %s' % ANT_ENV_TYPE)
        self.goal_sample_fn = get_goal_sample_fn(ANT_ENV_TYPE, random=random)
        self.reward_fn = get_reward_fn(ANT_ENV_TYPE)
        self.goal = None
        self.norm = norm
        MazeEnv.__init__(self, maze_id=maze_id, *args, **kwargs)
        self.prev_obs = self.reset()
        self.orig_obs = self.prev_obs

    def get_obs_labels(self):
        return MazeEnv.get_obs_labels(self) + ["goal"] * len(self.goal)

    def reset(self):
        obs = MazeEnv.reset(self)
        self.orig_obs = obs
        self.goal = self.goal_sample_fn()
        return np.concatenate([obs, self.goal])

    def step(self, a):
        prev_goal_dist = self.reward_fn(self.prev_obs, self.goal)
        obs, base_reward, done, info = MazeEnv.step(self, a)
        goal_dist = self.reward_fn(obs, self.goal)
        # goal_reward = goal_dist - prev_goal_dist
        goal_reward = {
                "none": goal_dist,
                "sub": goal_dist - self.reward_fn(self.orig_obs, self.goal),
        }[self.norm.lower()]
        # goal_reward = goal_dist - self.reward_fn(self.orig_obs, self.goal)
        # print("step", goal_dist, goal_reward)
        # goal_reward = goal_dist
        # Normalize wrt the original distance from the goal
        reward = base_reward + goal_reward
        info["goal_reward"] = goal_reward
        return np.concatenate([obs, self.goal]), goal_reward, done, info
