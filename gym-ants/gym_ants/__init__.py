from gym.envs.registration import register
from . import envs

register(
    id='CheetahPos-v2',
    entry_point='gym_ants.envs:HalfCheetahEnv',
    max_episode_steps=1000,
    reward_threshold=3600.0,
)

register(
    id='AntMaze-sanity-v0',
    entry_point='gym_ants.envs:AntMazeEnv',
    max_episode_steps=500,
    reward_threshold=3600.0,
    kwargs={'random': False, 'norm': 'sub'},
)

register(
    id='AntMaze-rand-v0',
    entry_point='gym_ants.envs:AntMazeEnv',
    max_episode_steps=500,
    reward_threshold=3600.0,
    kwargs={'random': True, 'norm': 'sub'},
)

register(
    id='AntMaze-v0',
    entry_point='gym_ants.envs:AntMazeEnv',
    max_episode_steps=500,
    reward_threshold=3600.0,
    kwargs={'random': False},
)

register(
    id='AntFall-v0',
    entry_point='gym_ants.envs:AntFallEnv',
    max_episode_steps=500,
    reward_threshold=3600.0,
)

register(
    id='AntPush-v0',
    entry_point='gym_ants.envs:AntPushEnv',
    max_episode_steps=500,
    reward_threshold=3600.0,
)

register(
    id='AntPush-joints-v0',
    entry_point='gym_ants.envs:AntPushEnv',
    max_episode_steps=500,
    reward_threshold=3600.0,
    kwargs={"expose_joint_coms":['root', 'hip_1', 'ankle_1', 'hip_2', 'ankle_2', 'hip_3', 'ankle_3', 'hip_4', 'ankle_4']},
)
