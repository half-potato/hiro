import setuptools

setuptools.setup(name='gym_ants',
    version='0.0.1',
    # install_requires=['gym', 'mujoco-py']  # And any other dependencies foo needs
    author="Alexander Mai",
    author_email="alexandertmai@gmail.com",
    description="Gym envs for hierarchical level RL",
    url="https://gitlab.com/half-potato/evalpy",
    packages=setuptools.find_packages(),
    package_data={'gym_ants': [
        'envs/assets/*.xml',
        ]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD 3-Clause License",
        "Operating System :: OS Independent",
    ],
)
