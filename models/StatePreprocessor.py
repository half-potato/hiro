import torch as th
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
import utils
from . import TD3
from icecream import ic
from torch.distributions.categorical import Categorical

def huber(x, kappa=0.1):
    linear = kappa * (th.abs(x) - 0.5 * kappa) * (tf.abs(x) > kappa).float()
    quad = 0.5 * th.square(x) * (th.abs(x) <= kappa).float()
    return ((linear + quad) / kappa).sum(-1)

class StatePreprocessor:
    def __init__(self, prior_replay_size=1024):
        self.norm = huber
        self.repr_fn = None
        self.aux_fn = None
        self.prior_replay_size = prior_replay_size
        self.prior_replay = th.zeros((self.prior_replay_size, embed_size))
        self.repr_optimizer = th.optim.Adam(list(self.repr_fn.parameters())+list(self.aux_fn.parameters()), lr=0.0001)

    def est_log_part(self, em_state, em_hl_actions):
        # shapes: (batch_size, feature_size)
        em_state_minibatch = self.repr_fn(self.prior_replay)
        D = self.norm((em_state+em_hl_actions)[:, None, :] - em_state_minibatch[None, :, :])
        return th.logsumexp(-D, -1)-th.log(em_state.shape[0])

    def low_level_reward(self, em_state, em_next_state, em_hl_actions, goals, log_part):
        # there is a mistake in the paper that labels the repulsive term as using
        # the state minibatch instead of next state, like it should
        return -self.norm((em_state + em_hl_actions) - goals) +
                self.norm((em_state + em_hl_actions) - em_next_state) +
                log_part

    def repr_loss(self, em_state, em_next_state, em_hl_actions, log_part):
        # calculated like in code
        attract_term = self.norm(em_state+em_hl_actions - em_next_state).mean()
        dist = self.norm((em_state+em_hl_actions)[1:] - em_next_state[:-1])
        repulsive_term = th.exp(-dist).mean() - log_part.detach()
        return attract_term + repulsive_term

    def sample_next_state_indicies(self, period, batch_size):
        # Sample with bias to lower period values
        probs = 0.99 ** th.range(period) * ([1.0] * (period - 1) + [1.0 / (1 - 0.99)])
        probs /= probs.sum()
        # Normal distribution that assigns marginally high probs to lower numbers
        index_dist = Categorical(probs)
        return index_dist.sample((batch_size)).long()

    def train(self, state_batch, hl_action_periods):
        # state_minibatch has arbitrary size, sampled independently for prior
        # sample next states
        # (batch_size, period, _)
        c = state_batch.shape[1]
        batch_size = state_batch.shape[0]

        initial_state = state_batch[:, 0, :] # initial state

        # Sample next states within each period
        rand_ind = self.sample_next_state_indicies(c, batch_size)
        next_state = state_batch[range(batch_size), rand_ind]


        # Embed elements
        em_state = self.repr_fn(initial_state)
        em_next_state = self.repr_fn(next_state)
        em_hl_actions = self.aux_fn(initial_state, hl_action_periods)

        log_part = self.est_log_part(em_state, em_hl_actions)
        loss = self.repr_loss(em_state, em_next_state, em_hl_actions, log_part)

        # Run training

        # Update replay
        self.prior_replay = th.cat((prior_replay[:batch_size], em_state), dim=0)
        return log_part
