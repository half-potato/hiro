import numpy as np
import torch as th
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
import utils
from icecream import ic

device = th.device("cuda" if th.cuda.is_available() else "cpu")

# Implementation of Twin Delayed Deep Deterministic Policy Gradients (TD3)
# Paper: https://arxiv.org/abs/1802.09477

def init_layers(layers):
    for m in layers[-2:]:
        if type(m) == nn.Linear:
            th.nn.init.uniform_(m.weight, a=-0.003, b=0.003)
            m.bias.data.fill_(0.0)
    for m in layers[:-2]:
        if type(m) == nn.Linear:
            th.nn.init.xavier_uniform_(m.weight, gain=1./3)
            m.bias.data.fill_(0.0)

class Actor(nn.Module):
    def __init__(self, state_dim, action_dim):
        super(Actor, self).__init__()

        layers = [
            nn.Linear(state_dim, 300),
            nn.ReLU(),
            nn.Linear(300, 300),
            nn.ReLU(),
            nn.Linear(300, action_dim),
            nn.Tanh()]

        # init_layers(layers)
        self.net = nn.Sequential(*layers)

    def forward(self, x):
        return self.net(x)


class Critic(nn.Module):
    def __init__(self, state_dim, action_dim):
        super(Critic, self).__init__()
        self.action_layer = nn.Linear(action_dim, 400)
        self.q1 = self._make_critic(state_dim, 400)
        self.q2 = self._make_critic(state_dim, 400)

    def _make_critic(self, state_dim, action_dim):
        layers = [
            nn.Linear(state_dim + action_dim, 300),
            # nn.ReLU(),
            # nn.Linear(300, 300),
            nn.ReLU(),
            nn.Linear(300, 1)
        ]
        # init_layers(layers)
        net = nn.Sequential(*layers)
        return net

    def forward(self, x, u):
        act = self.action_layer(u)
        xu = th.cat([x, act], 1)

        return self.q1(xu), self.q2(xu)

    def Q1(self, x, u):
        act = self.action_layer(u)
        xu = th.cat([x, act], 1)
        return self.q1(xu)


class TD3(object):
    def __init__(self, state_dim, action_dim, action_spec=None, actor_lr=0.0001, critic_lr=0.001):
        self.actor = Actor(state_dim, action_dim).to(device)
        self.actor_target = Actor(state_dim, action_dim).to(device)
        self.actor_target.load_state_dict(self.actor.state_dict())
        self.actor_optimizer = th.optim.Adam(self.actor.parameters(), lr=actor_lr)

        self.critic = Critic(state_dim, action_dim).to(device)
        self.critic_target = Critic(state_dim, action_dim).to(device)
        self.critic_target.load_state_dict(self.critic.state_dict())
        self.critic_optimizer = th.optim.Adam(self.critic.parameters(), lr=critic_lr)

        self.action_spec = action_spec.to(device) # (2, action_dim). row 0 = max, row 1 = min
        self.state_dim = state_dim
        self.action_dim = action_dim


    def scale(self, action):
        if self.action_spec is None:
            return action
        else:
            mag = (self.action_spec[0] - self.action_spec[1])/2
            mean = (self.action_spec[0] + self.action_spec[1])/2
            return action * mag + mean


    def inv_scale(self, action):
        if self.action_spec is None:
            return action
        else:
            mag = (self.action_spec[0] - self.action_spec[1])/2
            mean = (self.action_spec[0] + self.action_spec[1])/2
            return (action - mean) / mag


    def select_action(self, state):
        if type(state) == np.ndarray:
            state = th.FloatTensor(state).to(device)
        # if len(state.shape) != 2:
            # state = state.reshape(1, -1)
        state = state.reshape(-1, self.state_dim)
        return self.scale(self.actor(state).reshape(-1,self.action_dim))


    def select_np_action(self, state):
        return self.select_action(state).detach().cpu().data.numpy()


    def train(self, sample_batch, update_policy, discount=0.99, tau=0.005, policy_noise=0.2,
              noise_clip=0.5, policy_freq=2, clip_noise=False):

        # Sample replay buffer 
        #x, y, u, r, d = replay_buffer.sample(batch_size)
        x, y, u, r, d = sample_batch

        # state = th.FloatTensor(x).to(device)
        # action = th.FloatTensor(u).to(device)
        # next_state = th.FloatTensor(y).to(device)
        # done = th.FloatTensor(1 - d).to(device)
        # reward = th.FloatTensor(r).to(device)

        state = x.reshape(-1, self.state_dim)
        action = u.reshape(-1, self.action_dim)
        next_state = y.reshape(-1, self.state_dim)
        done = (1 - d).reshape(-1, 1)
        reward = r.reshape(-1, 1)

        # Select action according to policy and add clipped noise
        next_action = self.select_action(next_state)
        noise = action.clone().reshape(-1, self.action_dim).data.normal_(0, policy_noise).to(device)
        # noise = action.data.normal(0, policy_noise).to(device)
        noise = noise.clamp(-noise_clip, noise_clip)#.reshape(next_action.shape)
        next_action = utils.clip(next_action + noise,
                                 self.action_spec[1],
                                 self.action_spec[0])

        # Compute the target Q value
        target_Q1, target_Q2 = self.critic_target(next_state, next_action)
        target_Q = th.min(target_Q1, target_Q2)
        target_Q = reward + (done * discount * target_Q).detach()

        # Get current Q estimates
        current_Q1, current_Q2 = self.critic(state, action)

        # Compute critic loss
        critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(current_Q2, target_Q)

        # Optimize the critic
        self.critic_optimizer.zero_grad()
        critic_loss.backward()
        self.critic_optimizer.step()

        # Delayed policy updates
        #if it % policy_freq == 0:
        if update_policy:

            # Compute actor loss
            # This one line is the original TD3 loss
            actions = self.select_action(state)

            def orig_loss(state, actions):
                return -self.critic.Q1(state, actions).mean()

            # HIRO loss
            def grad_diff_loss(state, actions):
                critic_values = self.critic.Q1(state, actions)
                dqda = th.autograd.grad(critic_values, actions, grad_outputs=th.ones_like(critic_values), retain_graph=True)[0]
                action_norm = th.norm(actions)
                return (((dqda+actions).detach() - actions)**2).mean()# + action_norm
            # Maybe clip gradient here
            # actor_loss = orig_loss(state, actions)
            actor_loss = grad_diff_loss(state, actions)
            # actor_loss = -critic_values.mean()# - th.log(std).mean()
            # Optimize the actor
            self.actor_optimizer.zero_grad()
            actor_loss.backward()
            self.actor_optimizer.step()

            # Update the frozen target models
            for param, target_param in zip(self.critic.parameters(), self.critic_target.parameters()):
                target_param.data.copy_(tau * param.data + (1 - tau) * target_param.data)

            for param, target_param in zip(self.actor.parameters(), self.actor_target.parameters()):
                target_param.data.copy_(tau * param.data + (1 - tau) * target_param.data)
            return critic_loss, actor_loss
        return critic_loss, None


    def save(self, filename, directory):
        th.save(self.actor.state_dict(), '%s/%s_actor.pth' % (directory, filename))
        th.save(self.critic.state_dict(), '%s/%s_critic.pth' % (directory, filename))


    def load(self, filename):
        if th.cuda.is_available():
            self.actor.load_state_dict(
                    th.load('%s_actor.pth' % (filename)))
            self.critic.load_state_dict(
                    th.load('%s_critic.pth' % (filename)))
        else:
            self.actor.load_state_dict(
                    th.load('%s_actor.pth' % (filename), 
                               map_location='cpu'))
            self.critic.load_state_dict(
                    th.load('%s_critic.pth' % (filename),
                               map_location='cpu'))
