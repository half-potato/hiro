import torch as th
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
import utils
from . import TD3
from icecream import ic
from torch.distributions.categorical import Categorical

device = th.device("cuda" if th.cuda.is_available() else "cpu")

# state space is the space in which a goal is represented in relation to the state
# net space is the space in which a goal is represented for the low level network
# The HL network outputs to net space
# the sampled states are used to calculate a prior prob for any state

def huber(x, kappa=0.1):
    linear = kappa * (th.abs(x) - 0.5 * kappa) * (th.abs(x) > kappa).float()
    quad = 0.5 * x**2 * (th.abs(x) <= kappa).float()
    return ((linear + quad) / kappa).sum(-1)

def make_repr_fn(state_dim, repr_dim):
    layers = [
        nn.Linear(state_dim, 100),
        nn.ReLU(),
        nn.Linear(100, 100),
        nn.ReLU(),
        nn.Linear(100, repr_dim),
        nn.Tanh()]
    net = nn.Sequential(*layers)
    return net

class AuxNet(nn.Module):
    def __init__(self, state_dim, repr_dim, action_dim, policy_freq):
        super(AuxNet, self).__init__()
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.policy_freq = policy_freq
        self.repr_dim = repr_dim
        layers = [
            nn.Linear(state_dim+self.action_dim*self.policy_freq, 400),
            nn.ReLU(),
            nn.Linear(400, 300),
            nn.ReLU(),
            nn.Linear(300, repr_dim),
            nn.Tanh()]
        self.net = nn.Sequential(*layers)

    def forward(self, initial_state, action_batch):
        initial_state = initial_state.reshape(-1, self.state_dim)
        action_batch = action_batch.reshape(-1, self.action_dim*self.policy_freq)
        inputs = th.cat((initial_state, action_batch), dim=1)
        return self.net(inputs)

class HIROv2(object):
    #terminology
    """
    * minibatch = a batch sampled independently labeled squiggle S
    * period = the collection of either c states or actions taken under a single goal
    """

    def __init__(self, state_dim, action_dim, max_action, max_goal, repr_dim=2, policy_freq=9, prior_replay_size=1024):

        self.max_goal = max_goal
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.policy_freq = policy_freq
        self.repr_dim = repr_dim
        self.prior_replay_size = prior_replay_size

        self.hl_policy = TD3.TD3(repr_dim, repr_dim, th.FloatTensor([[1, -1]]*repr_dim).t().to(device))
        # TODO: REMOVE
        self.ll_policy = TD3.TD3(2*repr_dim, action_dim, th.FloatTensor([[max_action, -max_action]]*action_dim).t().to(device))
        self.dist = th.nn.SmoothL1Loss()
        self.norm = huber
        self.repr_fn = make_repr_fn(self.state_dim, self.repr_dim).to(device)
        self.aux_fn = AuxNet(self.state_dim, self.repr_dim, self.action_dim, self.policy_freq).to(device)
        self.repr_optimizer = th.optim.Adam(list(self.repr_fn.parameters())+list(self.aux_fn.parameters()), lr=0.0001)

        self.prior_replay = th.zeros((self.prior_replay_size, self.repr_dim)).to(device)

    #===========================================================================
    #                          HIRO v2 Representation                          #
    #===========================================================================

    def est_log_part(self, aux, em_state_minibatch):
        # shapes: (batch_size, feature_size)
        D = self.norm(aux[:, None, :] - em_state_minibatch[None, :, :])
        return th.logsumexp(-D, -1)-th.log(th.tensor(aux.shape[0]).float())

    def low_level_reward(self, aux, em_next_state, goals, log_part):
        # there is a mistake in the paper that labels the repulsive term as using
        # the state minibatch instead of next state, like it should
        reward = -self.norm(aux - goals) + self.norm(aux - em_next_state) + log_part
        return reward.detach()

    def repr_loss(self, aux, em_next_state, log_part, em_state_minibatch=None):
        if em_state_minibatch is None:
            # calculated like in code
            dist = self.norm(aux[1:] - em_next_state[:-1])
        else:
            # calculated like in paper
            dist = self.norm(aux - em_state_minibatch)
        attract_term = self.norm(aux - em_next_state).mean()
        repulsive_term = (th.exp(-dist) - log_part.detach()[1:]).mean()
        return attract_term + repulsive_term

    def sample_next_state_indicies(self, period, batch_size):
        # Sample with bias to lower period values
        probs = 0.99 ** th.linspace(0, period-2, period-1) * th.tensor([1.0] * (period - 2) + [1.0 / (1 - 0.99)])
        probs /= probs.sum()
        # Normal distribution that assigns marginally high probs to lower numbers
        index_dist = Categorical(probs)
        indices = index_dist.sample([batch_size]).long()
        return indices, indices+1

    #===========================================================================
    #                            State Manipulation                            #
    #===========================================================================


    # To be run in between updating goal
    def interp_goal(self, prev_state, state, prev_goal):
        prev_state = prev_state.reshape(-1, self.state_dim)
        state = state.reshape(-1, self.state_dim)
        prev_goal = prev_goal.reshape(-1, self.repr_dim)
        em_prev_state = self.repr_fn(prev_state)
        em_state = self.repr_fn(state)

        # We want the next goal to be zero in the slots it doesn't care about
        return em_prev_state - em_state + prev_goal.to(device)


    def merge(self, em_state, goal):
        em_state = em_state.reshape(-1, self.repr_dim)
        goal = goal.reshape(-1, self.repr_dim)
        gs = th.cat((em_state, goal), dim=1)
        return gs

    #===========================================================================
    #                        Outward Facing Functions                          #
    #===========================================================================


    # To be run every C steps, outputs to state space
    def select_goal(self, state):
        # Cast to shape [batch_size, state_size]
        state = state.reshape(-1, self.state_dim)
        em_state = self.repr_fn(state)
        # Get goal and scale
        val = self.hl_policy.select_action(em_state)
        # return 1*th.ones_like(val).to(device)
        return val


    # Recieves goal in state space
    def select_action(self, state, goal):
        state = state.reshape(-1, self.state_dim)
        goal = goal.reshape(-1, self.repr_dim)
        em_state = self.repr_fn(state)

        # scaled_goal = self.inv_scale_goal(goal)
        # scaled_goal = th.zeros_like(scaled_goal)
        return self.ll_policy.select_action(self.merge(em_state, goal)).detach()

    # Recieves goal in state space
    def select_np_action(self, state, goal):
        return self.select_action(state, goal).cpu().numpy()


    def train_hl(self, hl_sample_batch, update_policy, correction_sample_size=10,
                 discount=0.99, tau=0.005, policy_noise=0.2, noise_clip=0.5):

        x, y, _, _, r, d, action_batch = hl_sample_batch
        batch_size = x.shape[0]

        state_batch = x.reshape(-1, self.policy_freq, self.state_dim)
        last_state = y.reshape(-1, self.state_dim)
        action_batch = action_batch.reshape(-1, self.policy_freq, self.action_dim)
        d = d.reshape(-1, 1) # because the done vars are 1 d

        initial_state = state_batch[:, 0, :] # initial state
        em_init_state = self.repr_fn(initial_state)
        em_last_state = self.repr_fn(last_state)

        # Discount
        discount_m = (discount**th.arange(0, self.policy_freq-1)).type(th.FloatTensor).to(device).repeat(batch_size, 1)
        discount_reward = discount_m * th.squeeze(r[:, :-1])
        r_sum = th.sum(discount_reward, dim=1).reshape(-1, 1).detach()

        reproj_goals = self.aux_fn(initial_state, action_batch)

        reward_scale = 0.1
        critic_loss, actor_loss = self.hl_policy.train((em_init_state.detach(),
            em_last_state.detach(), reproj_goals.detach(), reward_scale*r_sum, d.detach()),
            update_policy, discount, tau, policy_noise, noise_clip)
        # critic_loss, actor_loss = self.hl_policy.train((self.norm_obs(initial_state),
            # self.norm_obs(y), initial_goal, reward_scale*r_sum, d),
            # update_policy, discount, tau, policy_noise, noise_clip)
        return critic_loss, actor_loss


    def train_ll(self, ll_sample_batch, update_policy, discount=0.99, tau=0.005,
              policy_noise=0.2, noise_clip=0.5):
        # High level = \pi_{hi}
        # Low level = \pi_{lo}
        # state = s_{t+k-1}, next_state = s_{t+k}
        # "Sample batch of c-step transitions"
        x, y, g, h, _, d, action_batch = ll_sample_batch
        batch_size = g.shape[0]

        state_batch = x.reshape(-1, self.policy_freq, self.state_dim)
        action_batch = action_batch.reshape(-1, self.policy_freq, self.action_dim)
        d = d.reshape(-1, 1) # because the done vars are 1 d

        initial_state = state_batch[:, 0, :] # initial state

        # Sample which states are selected to be the next states
        # "Sample indicies into transition"
        ind, next_ind = self.sample_next_state_indicies(self.policy_freq, batch_size)
        state = state_batch[range(batch_size), ind]
        next_state = state_batch[range(batch_size), next_ind]
        action = action_batch[range(batch_size), ind]
        goals = g[range(batch_size), ind]
        next_goals = g[range(batch_size), next_ind]

        # Embed elements
        em_init_state = self.repr_fn(initial_state)
        em_state = self.repr_fn(state)
        em_next_state = self.repr_fn(next_state)
        # Every time s_t, a_{t:t+c-1} is passed, it's passed into the aux fn, so we preprocess
        # TODO Check if this is true
        # \phi_{\theta}(s_t, a_{t:t+c-1}) = aux(s_t, a_{t:t+c-1}) + repr_fn(s_t)
        em_hl_actions = self.aux_fn(initial_state, action_batch)
        aux = em_hl_actions + em_init_state

        # "Estimate log-partitions"
        # "Sample batch of states"
        log_part = self.est_log_part(aux, self.prior_replay)
        # "Compute loss"
        loss = self.repr_loss(aux, em_next_state, log_part)

        # Repr training
        self.repr_optimizer.zero_grad()
        loss.backward()
        self.repr_optimizer.step()

        # LL training
        xg = self.merge(em_state, goals)
        yh = self.merge(em_next_state, next_goals)
        # "Compute low level rewards"
        reward = self.low_level_reward(aux, em_next_state, goals, log_part)
        critic_loss, actor_loss = self.ll_policy.train((xg.detach(), yh.detach(), action.detach(), reward, d),
                update_policy, discount, tau, policy_noise, noise_clip)

        # Update replay
        self.prior_replay = th.cat((self.prior_replay[:batch_size], em_next_state), dim=0)
        return float(reward.mean()), loss, critic_loss, actor_loss


    def save(self, file_name):
        self.ll_policy.save(file_name+"_ll")
        self.hl_policy.save(file_name+"_hl")


    def load(self, file_name):
        self.ll_policy.load(file_name+"_ll")
        self.hl_policy.load(file_name+"_hl")
