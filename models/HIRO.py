import torch as th
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
import utils
from . import TD3
from icecream import ic

device = th.device("cuda" if th.cuda.is_available() else "cpu")

# state space is the space in which a goal is represented in relation to the state
# net space is the space in which a goal is represented for the low level network
# The HL network outputs to net space

# Zero out xy pos for low level policy
def zero_pos(state):
    mask = th.ones_like(state)
    mask[:,:2] = 0
    return mask * state

class HIRO(object):

    def __init__(self, state_dim, action_dim, goal_bounds, state_stats, max_action, max_goal):
        self.goal_bounds = goal_bounds
        self.max_goal = max_goal
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.goal_dim = goal_bounds.shape[1]
        self.state_mean = state_stats[0].reshape(-1, self.state_dim)
        self.state_std = state_stats[1].reshape(-1, self.state_dim)
        self.hl_policy = TD3.TD3(state_dim, self.goal_dim, goal_bounds)
        # TODO: REMOVE
        self.ll_policy = TD3.TD3(state_dim+self.goal_dim, action_dim, th.FloatTensor([[max_action, -max_action]]*action_dim).t().to(device))
        self.loss = th.nn.SmoothL1Loss()


    def norm_obs(self, obs):
        # obs = obs.reshape(-1, self.state_dim)
        # mean = th.repeat(self.state_mean, obs.shape[0], dim=0)
        # std = th.repeat(self.state_std, obs.shape[0], dim=0)
        # return (obs - mean) / std
        return obs


    # Pad goal to be size of state
    def pad_goal(self, prev_goal, current_state):
        current_state = current_state.reshape(-1, self.state_dim)
        prev_goal = prev_goal.reshape(-1, self.goal_dim)

        goal_w = prev_goal.shape[1]
        padded_goal = th.cat((prev_goal, current_state[:, goal_w:]), dim=1)
        return padded_goal


    # To be run in between updating goal
    def interp_goal(self, prev_state, state, prev_goal):
        prev_state = prev_state.reshape(-1, self.state_dim)
        state = state.reshape(-1, self.state_dim)
        prev_goal = prev_goal.reshape(-1, self.goal_dim)

        # We want the next goal to be zero in the slots it doesn't care about
        interp = prev_state - state + self.pad_goal(prev_goal, state - prev_state)
        return interp[:, :prev_goal.shape[1]]


    def merge(self, state, goal):
        state = state.reshape(-1, self.state_dim)
        goal = goal.reshape(-1, self.goal_dim)
        gs = th.cat((zero_pos(state), goal), dim=1)
        return gs


    def ll_reward(self, prev_state, state, prev_goal):
        prev_state = prev_state.reshape(-1, self.state_dim)
        state = state.reshape(-1, self.state_dim)
        prev_goal = prev_goal.reshape(-1, self.goal_dim)
        # r = th.sum((prev_state[:, :self.goal_dim] - state[:, :self.goal_dim] + prev_goal)**2, dim=1)
        # return - th.sqrt(r).reshape(-1, 1)
        return -self.loss(prev_state[:, :self.goal_dim] + prev_goal, state[:, :self.goal_dim])


    # To be run every C steps, outputs to state space
    def select_goal(self, state):
        # Cast to shape [batch_size, state_size]
        state = self.norm_obs(state.reshape(-1, self.state_dim))
        # Get goal and scale
        val = self.hl_policy.select_action(state)
        # return self.scale_goal(val)
        # return th.FloatTensor([[1, 1]]).to(device)
        return 1*th.ones_like(val).to(device)
        # return th.array([[1]])
        # return val


    # Recieves goal in state space
    def select_action(self, state, goal):
        state = self.norm_obs(state.reshape(-1, self.state_dim))
        goal = goal.reshape(-1, self.goal_dim)

        # scaled_goal = self.inv_scale_goal(goal)
        # scaled_goal = th.zeros_like(scaled_goal)
        return self.ll_policy.select_action(self.merge(state, goal)).detach()

    # Recieves goal in state space
    def select_np_action(self, state, goal):
        state = self.norm_obs(state.reshape(-1, self.state_dim))
        goal = goal.reshape(-1, self.goal_dim)

        # scaled_goal = self.inv_scale_goal(goal)
        # scaled_goal = th.zeros_like(scaled_goal)
        return self.ll_policy.select_np_action(self.merge(state, goal))

    def get_sample_goals(self, initial_state, final_state, initial_goal, correction_sample_size):
        # Sample goals
        center = final_state[:,:self.goal_dim]-initial_state[:,:self.goal_dim]
        goals = th.normal(center.repeat(correction_sample_size-2, 1), self.max_goal/4)
        # goals = self.scale_goal(goals.clip(-self.max_goal, self.max_goal))

        goals = th.cat((initial_goal, center, goals), dim=0)
        return goals

    def goal_reprojection(self, hl_sample_batch, correction_sample_size=10, discount=0.99):
        # Perform correction
        x, y, g, _, _, u = hl_sample_batch
        # ic(x.shape, y.shape, g.shape, r.shape, d.shape, u.shape)

        # ic| x.shape: (100, 10, 32)
            # y.shape: (100, 32)
            # g.shape: (100, 10, 32)
            # r.shape: (100, 10)
            # d.shape: (100,)
            # u.shape: (100, 10, 8)

        batch_size = g.shape[0]
        policy_freq = x.shape[1]
        action_size = u.shape[2]

        x = x.reshape(-1, policy_freq, self.state_dim)
        y = y.reshape(-1, self.state_dim)
        g = g.reshape(-1, policy_freq, self.goal_dim)
        u = u.reshape(-1, policy_freq, self.action_dim)

        initial_state = x[:, 0, :] # initial state
        initial_goal = g[:, 0, :] # initial goal, uncorrected

        # Set goals to maximize the chance of the corresponding action u being performed
        # sample the goal itself as well

        # Summer for use in adding up errors in action reproduction
        w = correction_sample_size*policy_freq
        summer = th.triu(th.tril(th.ones((w, w)), policy_freq-1), 0)
        # for i in range(policy_freq):
            # s_i = 1 - (1-discount)*th.triu(th.tril(th.ones((w, w)), i), 0)
            # summer *= s_i
        summer = summer.to(device)

        reproj_error = 0
        reproj_goals = th.zeros_like(th.squeeze(g[:, 0, :]))
        for i in range(batch_size):
            # Sample goals
            goals = self.get_sample_goals(initial_state[i, None], y[i, None], initial_goal[i, None], correction_sample_size)

            # Tile to match sizes
            initial_goals = goals.repeat(policy_freq, 1).reshape(-1, self.goal_dim)
            # np.repeat(goals, policy_freq, dim=0).reshape(-1, self.goal_dim)
            # initial_states = th.tile(
                    # initial_state[i, None],
                    # (policy_freq*correction_sample_size, 1, 1)).reshape(-1, self.state_dim)
            initial_states = initial_state[i, None].repeat(policy_freq*correction_sample_size, 1, 1).reshape(-1, self.state_dim)
            # states = th.tile(x[i], (correction_sample_size, 1, 1)).reshape(-1, self.state_dim)
            # actions = th.tile(u[i], (correction_sample_size, 1, 1)).reshape(-1, action_size)
            states = x[i].repeat(correction_sample_size, 1, 1).reshape(-1, self.state_dim)
            actions = u[i].repeat(correction_sample_size, 1, 1).reshape(-1, action_size)

            # Apply goal transition function.
            interped_goals = self.interp_goal(initial_states, states, initial_goals)

            # Feed
            states = self.norm_obs(states)
            output = self.select_action(states, interped_goals)

            # Get best goal
            resamp_diff = th.mean((output - actions)**2, 1).reshape(-1, 1)
            # Sum over each sequence to get best match
            # TODO Add reward discounting
            summed_diff = (summer.mm(resamp_diff))[::policy_freq]
            j = th.argmin(summed_diff)
            reproj_goals[j] = goals[j]

            reproj_error += th.mean(summed_diff[j]**2)

        reproj_error /= batch_size
        return reproj_goals.reshape(-1, self.goal_dim), reproj_error


    def train_hl(self, hl_sample_batch, update_policy, correction_sample_size=10,
                 discount=0.99, tau=0.005, policy_noise=0.2, noise_clip=0.5):
        x, y, g, r, d, u = hl_sample_batch
        batch_size = g.shape[0]
        policy_freq = x.shape[1]
        action_size = u.shape[2]

        y = y.reshape(-1, self.state_dim)
        g = g.reshape(-1, policy_freq, self.goal_dim)
        u = u.reshape(-1, policy_freq, self.action_dim)
        d = d.reshape(-1, 1) # because the done vars are 1 d
        r = r.reshape(-1, policy_freq, 1) # because the done vars are 1 d

        initial_state = x[:, 0, :] # initial state
        initial_goal = g[:, 0, :] # initial goal, uncorrected
        # Discount
        discount_m = (discount**th.arange(0, policy_freq-1)).type(th.FloatTensor).to(device).repeat(batch_size, 1)
        discount_reward = discount_m * th.squeeze(r[:, :-1])
        r_sum = th.sum(discount_reward, dim=1).reshape(-1, 1).detach()

        reproj_goals, reproj_error = self.goal_reprojection(hl_sample_batch, correction_sample_size)
        # reproj_error = 0

        reward_scale = 0.1
        critic_loss, actor_loss = self.hl_policy.train((self.norm_obs(initial_state).detach(),
            self.norm_obs(y).detach(), reproj_goals.detach(), reward_scale*r_sum, d.detach()),
            update_policy, discount, tau, policy_noise, noise_clip)
        # critic_loss, actor_loss = self.hl_policy.train((self.norm_obs(initial_state),
            # self.norm_obs(y), initial_goal, reward_scale*r_sum, d),
            # update_policy, discount, tau, policy_noise, noise_clip)
        return reproj_error, critic_loss, actor_loss


    def train_ll(self, ll_sample_batch, update_policy, discount=0.99, tau=0.005,
              policy_noise=0.2, noise_clip=0.5):
        # Calculate reward for lower level policy
        x, y, g, h, d, u, r = ll_sample_batch
        d = d.reshape(-1, 1).detach()
        x = x.reshape(-1, self.state_dim).detach()
        y = y.reshape(-1, self.state_dim).detach()
        g = g.reshape(-1, self.goal_dim).detach()
        h = h.reshape(-1, self.goal_dim).detach()
        # ns_g = self.inv_scale_goal(g)
        # ns_h = self.inv_scale_goal(h)
        # adjusted low level reward: how well did it reach the goal state
        r2 = self.ll_reward(x, y, g).detach()
        # r3 = self.ll_reward(x, y, h)
        # print(r[:3], r2[:3], r3[:3])
        # Concat observation with goal
        xg = self.merge(x, g).detach()
        yh = self.merge(y, h).detach()

        critic_loss, actor_loss = self.ll_policy.train((xg, yh, u, r2, d),
                update_policy, discount, tau, policy_noise, noise_clip)
        return th.mean(r), critic_loss, actor_loss


    def save(self, file_name):
        self.ll_policy.save(file_name+"_ll")
        self.hl_policy.save(file_name+"_hl")


    def load(self, file_name):
        self.ll_policy.load(file_name+"_ll")
        self.hl_policy.load(file_name+"_hl")
